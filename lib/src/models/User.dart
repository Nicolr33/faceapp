class User {
  final int id;
  final String name;
  final String img_url;
  final String email;
  final String created_at;
  final String updated_at;

  User({this.id, this.name, this.img_url, this.email, this.created_at, this.updated_at});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      name: json['name'],
      img_url: json['img_url'],
      email: json['email'],
      created_at: json['created_at'],
      updated_at: json['updated_at']
    );
  }
}