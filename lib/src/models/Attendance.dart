import 'package:app/src/models/User.dart';

class Attendance {
  final int id;
  final int user_id;
  final String comment;
  final int type;
  final String access_at;
  final String lat;
  final String lng;
  final String address;
  final User user;

  Attendance({this.id, this.user_id, this.comment, this.type, this.lat, this.lng, this.access_at, this.address, this.user});

  factory Attendance.fromJson(Map<String, dynamic> json) {
    return Attendance(
      id: json['id'],
      user_id: json['user_id'],
      comment: json['comment'],
      type: json['type'],
      lat: json['lat'],
      lng: json['lng'],
      access_at: json['access_at'],
      address: json['address'],
      user: User.fromJson(json["user"])
    );
  }
}