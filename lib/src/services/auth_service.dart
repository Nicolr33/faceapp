import 'dart:io';
import 'dart:typed_data';

import 'package:app/src/models/User.dart';
import 'package:app/src/pages/admin/admin_pdf_report_page.dart';
import 'package:app/src/pages/home/home_page.dart';
import 'package:app/src/services/attendance_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  static final API_URL = "https://faceapi.kibou.es/api/";

  static void signIn(
      String email, String password, BuildContext context) async {
    Map body = {"email": email, "password": password};

    http.Response res = await http.post(API_URL + "login",
        body: json.encode(body),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });

    if (res.statusCode == 200) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = json.decode(res.body)['token'];
      await prefs.setString('token', token);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => HomePage(
                    user: getAuthUser(context),
                    attendances:
                        AttendanceService.getAttendancesFromUser(context),
                    isAdmin: getRole(context),
                  )),
          (route) => false);
    } else {
      throw Exception('Fallo al iniciar sesión');
    }
  }

  static Future<User> getAuthUser(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.get(API_URL + "user",
        headers: <String, String>{'Authorization': 'Bearer $token'});

    if (res.statusCode == 200) {
      return User.fromJson(json.decode(res.body)['user']);
    } else if (res.statusCode == 401) {
      prefs.remove('token');
      Navigator.pushReplacementNamed(context, 'login');
    } else {
      throw Exception("Failed to get user");
    }
  }

  static Future<bool> getRole(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.get(API_URL + "user",
        headers: <String, String>{'Authorization': 'Bearer $token'});

    if (res.statusCode == 200) {
      if (json.decode(res.body)['role'] != null) {
        return true;
      }
    }

    return false;
  }

  static void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    Navigator.pushReplacementNamed(context, 'login');
  }

  static void changePassword(BuildContext context, String password) async {
    Map body = {"password": password};
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.post(API_URL + "user/changePassword",
        body: json.encode(body),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        });
    print(res.body);
    if (res.statusCode == 200) {
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: "Se ha cambiado la contraseña correctamente.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      throw Exception('Fallo al cambiar la contraseña.');
    }
  }

  static void updateUser(int id, String name, String email, BuildContext context) async {
    Map body = {"name": name, "email": email, "user_id": id};
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.post(API_URL + "admin/users/edit",
        body: json.encode(body),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        });

    if (res.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "Se ha cambiado el usuario correctamente.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
          Navigator.pop(context);
    } else {
      throw Exception('Fallo al actualizar el usuario.');
    }
  }

  static void createUser(String name, String email, String password, BuildContext context) async {
    Map body = {"name": name, "email": email, "password": password};
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.put(API_URL + "admin/users/create",
        body: json.encode(body),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        });

    print(res.body);
    if (res.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "Se ha creado el usuario correctamente.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
          Navigator.pop(context);
    } else {
      throw Exception('Fallo al crear el usuario.');
    }
  }

  static void downloadReport(int id, BuildContext context) async {
    Map body = {"user_id": id,};
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.post(API_URL + "admin/users/downloadReport",
        body: json.encode(body),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        });

    print(res.body);
    if (res.statusCode == 200) {
      writeCounter(res.bodyBytes);
      String path = (await _localFile).path;
      Navigator.push(context, MaterialPageRoute(builder: (context) => AdminPDFReportPage(path: path)));
      Fluttertoast.showToast(
          msg: "Se ha descargado el informe correctamente.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      throw Exception('Fallo al crear el usuario.');
    }
  }

  static Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  static Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/report.pdf');
  }

  static Future<File> writeCounter(Uint8List stream) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsBytes(stream);
  }
}
