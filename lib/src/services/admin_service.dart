import 'dart:convert';

import 'package:app/src/models/Attendance.dart';
import 'package:app/src/models/User.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class AdminService {
  static final API_URL = "https://faceapi.kibou.es/api/";
  
  static Future<List<User>> getUsers(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.get(API_URL + "admin/users",
        headers: <String, String>{'Authorization': 'Bearer $token'});

    if (res.statusCode == 200) {
      return List<User>.from(
          json.decode(res.body).map((x) => User.fromJson(x)));
    } else if (res.statusCode == 401) {
      Navigator.pop(context);
    } else {
      throw Exception("Failed to get users");
    }
  }

  static Future<List<Attendance>> getAttendances(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.get(API_URL + "admin/attendances",
        headers: <String, String>{'Authorization': 'Bearer $token'});

    if (res.statusCode == 200) {
      return List<Attendance>.from(
          json.decode(res.body).map((x) => Attendance.fromJson(x)));
    } else if (res.statusCode == 401) {
      Navigator.pop(context);
    } else {
      throw Exception("Failed to get attendances");
    }
  }
}
