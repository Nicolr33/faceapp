import 'dart:convert';
import 'dart:io';

import 'package:app/src/models/Attendance.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class AttendanceService {
  static final API_URL = "https://faceapi.kibou.es/api/";

  static Future<List<Attendance>> getAttendancesFromUser(
      BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    http.Response res = await http.get(API_URL + "attendances",
        headers: <String, String>{'Authorization': 'Bearer $token'});

    print(res.statusCode);
    if (res.statusCode == 200) {
      return List<Attendance>.from(
          json.decode(res.body).map((x) => Attendance.fromJson(x)));
    } else if (res.statusCode == 401) {
      prefs.remove('token');
      Navigator.pushNamedAndRemoveUntil(context, 'login', null);
    } else {
      throw Exception("Failed to get attendances");
    }
  }

  static Future<void> setAttendance(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    var location = new Location();
    LocationData locationData = await location.getLocation();
    final coordinates =
        new Coordinates(locationData.latitude, locationData.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);

    print(addresses.first.addressLine);
    Map body = {
      "lat": locationData.latitude,
      "lng": locationData.longitude,
      "address": addresses.first.addressLine
    };
    http.Response res = await http.post(API_URL + "attendances",
        headers: <String, String>{
          'Authorization': 'Bearer $token',
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(body));
    print(res.body);
    if (res.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "Se ha fichado correctamente.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      Fluttertoast.showToast(
          msg: "No se ha fichado correctamente.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      throw Exception("Failed to get attendances");
    }
  }
}
