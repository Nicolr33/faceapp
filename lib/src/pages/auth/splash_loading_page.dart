import 'dart:math';

import 'package:app/src/pages/bottom_nav.dart';
import 'package:app/src/pages/home/home_page.dart';
import 'package:app/src/services/attendance_service.dart';
import 'package:app/src/services/auth_service.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashLoadingPage extends StatefulWidget {
  @override
  _SplashLoadingPageState createState() => _SplashLoadingPageState();
}

class _SplashLoadingPageState extends State<SplashLoadingPage> {
  @override
  String initText = "...";

  List<String> randomText = [
    "Inicializando alumnos...",
    "Inicializando profesores...",
    "Inicializando asignaturas...",
    "Suspendiendo a algun alumno aleatorio...",
  ];

  void randomString() {
    var rng = new Random();
    initText = randomText[rng.nextInt(randomText.length)];
  }

  void initState() {
    super.initState();
    randomString();
    checkHasToken();
  }

  void checkHasToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String token = prefs.getString("token");
    Future.delayed(const Duration(milliseconds: 2000), () {
        setState(() {
          if (token != null) {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage(user: AuthService.getAuthUser(context), attendances: AttendanceService.getAttendancesFromUser(context), isAdmin: AuthService.getRole(context),)));
          } else {
            Navigator.pushNamed(context, "login");
          }
        });
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: HexColor("27519E")),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 50.0,
                        backgroundImage: NetworkImage(
                            "https://cdn.discordapp.com/attachments/656314735683305473/707677371389706261/logo_ECAIB_NET_1.png"),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      Text(
                        "Gestion Poblenou",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 24.0),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Text(
                      initText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                          color: Colors.white),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
