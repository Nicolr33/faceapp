import 'package:app/src/services/auth_service.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("27519E"),
        elevation: 0.0,
        title: Text(
          "Ajustes",
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        color: HexColor("F2F4F8"),
        child: ListView(
          
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Cuenta',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: ListTile(
                  title: Text(
                    'Cambiar contraseña',
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 12,
                  ),
                  onTap: () => Navigator.pushNamed(context, 'password')
                ),
              ),
            ),
            Divider(
              height: 2,
              color: Colors.black12,
            ),
            Container(height: 10.0, color: Colors.black12),
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Sobre la aplicación',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: ListTile(
                  title: Text(
                    'Version de la aplicación',
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Text("1.0.2"),
                ),
              ),
            ),
            Divider(
              height: 2,
              color: Colors.black12,
            ),
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: ListTile(
                  title: Text(
                    'Terminos de condiciones',
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 12,
                  ),
                  onTap: () => {}
                ),
              ),
            ),
                        Divider(
              height: 2,
              color: Colors.black12,
            ),
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: ListTile(
                  title: Text(
                    'Aviso legal',
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 12,
                  ),
                  onTap: () => {}
                ),
              ),
            ),
            Container(height: 10.0, color: Colors.black12),
            Align(
              alignment: FractionalOffset.bottomCenter,
              child: Container(
                color: HexColor("27519E"),
                  child: Material(
                    type: MaterialType.transparency,
                    child: ListTile(
                      title: Center(
                        child: Text(
                          'Cerrar sesión',
                          style: TextStyle(fontSize: 15.0, color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      onTap: () => AuthService.logout(context),
                    ),
                  ),
                ),
            ),
              Divider(
                height: 2,
                color: Colors.black12,
              ),
              Expanded(
                child: Container(
                  color: Colors.black26,
                ),
              )

          ],
        ),
      ),
    );
  }
}
