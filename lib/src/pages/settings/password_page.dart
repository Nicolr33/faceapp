import 'package:app/src/services/auth_service.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

class PasswordPage extends StatefulWidget {
  @override
  _PasswordPageState createState() => _PasswordPageState();
}

class _PasswordPageState extends State<PasswordPage> {
  var _passwordController = TextEditingController();

  final formKey = new GlobalKey<FormState>();

    bool validateAndSave() {
    final form = formKey.currentState;
    print(form);
    if (form.validate()) {
      form.save();
      AuthService.changePassword(context, _passwordController.text);
      return true;
    }
    return false;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("27519E"),
        elevation: 0.0,
        title: Text(
          "Cambiar contraseña",
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(left: 17.0, top: 20.0),
              child: Text(
                'Contraseña',
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              )),
          Container(
            margin: EdgeInsets.only(left: 17.0, top: 8.0),
            child: Text(
              'Escribe la contraseña que deseas tener.',
              style: TextStyle(color: Colors.black),
            ),
          ),
          Form(
            key: formKey,
            child: Container(
                margin: EdgeInsets.only(
                    top: 15.0, right: 15.0, left: 15.0, bottom: 10.0),
                child: TextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    validator: (value) => value.isEmpty
                        ? 'Tienes que introducir una contraseña'
                        : null,
                    decoration: InputDecoration(
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius:
                            BorderRadius.circular(ScreenUtil().setWidth(5)),
                      ),
                      hintText: 'Contraseña',
                    ),
                    onSaved: (value) {
                      _passwordController.text = value;
                    })),
          ),
          Center(
            child: Container(
              child: MaterialButton(
                child: Text(
                  'Actualizar contraseña',
                  style: TextStyle(color: Colors.white),
                ),
                color: Theme.of(context).primaryColor,
                onPressed: () => validateAndSave(),
                elevation: 7.0,
                height: ScreenUtil().setHeight(80.0),
                shape: StadiumBorder(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
