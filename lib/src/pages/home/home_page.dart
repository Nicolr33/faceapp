import 'dart:collection';

import 'package:app/src/models/Attendance.dart';
import 'package:app/src/models/User.dart';
import 'package:app/src/services/attendance_service.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:app/src/utils/biometric_util.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:permission_handler/permission_handler.dart';

class HomePage extends StatefulWidget {
  final Future<User> user;
  final Future<List<Attendance>> attendances;
  final Future<bool> isAdmin;
  HomePage({Key key, this.user, this.attendances, this.isAdmin}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<List<Attendance>> attendances;
  bool isAdmin;

  List<String> dateString = new List();
  bool isWeekend() {
    DateTime date = DateTime.now();

    return (date.weekday == 6 || date.weekday == 7);
  }

  void initState() {
    initDate();
    setAttendances();
    requestPermission();
    super.initState();
  }

  void initDate() {
    DateTime date = DateTime.now();
    initializeDateFormatting('es').then((_) {
      setState(() {
        dateString.add(DateFormat('EEEE', 'es_ES').format(date));
        dateString.add(DateFormat('d', 'es_ES').format(date));
        dateString.add(DateFormat('MMMM', 'es_ES').format(date));
        dateString.add(DateFormat('yyyy', 'es_ES').format(date));
      });
    });
  }

  void setAttendances() async {
    bool isAdmin = await widget.isAdmin;
    setState(() {
      attendances = widget.attendances;
      this.isAdmin = isAdmin;
    });
  }

  void requestPermission() async {
    var status = await Permission.location.status;
    if (status.isDenied || status.isUndetermined) {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.location,
      ].request();
      print(statuses[Permission.location]);
    }
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('¡Oh vaya!'),
            content: new Text('¿Ya quieres cerrar la aplicación?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Si'),
              ),
            ],
          ),
        ) ??
        false;
  }

  void showAttendanceDetail(Attendance attendance) {
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: context,
        builder: (BuildContext context) {
          return Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                attendance.type == 0
                    ? Icon(
                        Icons.open_in_browser,
                        size: 42.0,
                        color: Colors.grey,
                      )
                    : Icon(
                        Icons.exit_to_app,
                        size: 42.0,
                        color: Colors.grey,
                      ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  child: attendance.type == 0
                      ? Text(
                          "Entrada",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24.0),
                        )
                      : Text(
                          "Salida",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24.0),
                        ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  attendance.access_at.split(" ")[0],
                  style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Divider(
                        height: 1.0,
                        color: Colors.black38,
                      ),
                      ListTile(
                        title: Text(
                          "Tipo: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle:
                            Text(attendance.type == 0 ? "Entrada" : "Salida"),
                      ),
                      Divider(
                        height: 1.0,
                        color: Colors.black38,
                      ),
                      ListTile(
                        title: Text(
                          "Comentario: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(attendance.comment == null ||
                                attendance.comment.isEmpty
                            ? "No hay ningun comentario."
                            : attendance.comment),
                      ),
                      Divider(
                        height: 1.0,
                        color: Colors.black38,
                      ),
                      ListTile(
                        title: Text(
                          "Dirección: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(attendance.address == null ||
                                attendance.address.isEmpty
                            ? "No se ha encontrado una dirección valida."
                            : attendance.address),
                      ),
                      Divider(
                        height: 1.0,
                        color: Colors.black38,
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: HexColor("27519E"),
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () => Navigator.pushNamed(context, 'settings'),
            ),
            isAdmin != null && isAdmin ? IconButton(
              icon: Icon(Icons.airline_seat_recline_extra),
              onPressed: () => Navigator.pushNamed(context, 'adminhome'),
            ) : Container()
          ],
        ),
        body: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    HexColor("27519E"),
                    HexColor("36578E"),
                    HexColor("455D7E"),
                  ],
                      stops: [
                    0.1,
                    0.5,
                    0.9
                  ])),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10.0),
              child: Column(
                children: <Widget>[
                  FutureBuilder<User>(
                    builder: (context, snapshot) {
                      var user = snapshot.data;
                      if (!snapshot.hasData) {
                        return CircularProgressIndicator();
                      }

                      return Column(
                        children: <Widget>[
                          CircleAvatar(
                            radius: 40.0,
                            backgroundImage: NetworkImage(
                                snapshot.hasData ? user.img_url : null),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(5.0),
                          ),
                          Text(
                            'Bienvenido, ${!snapshot.hasData ? "usuario" : user.name} ',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      );
                    },
                    future: widget.user,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(5.0),
                  ),
                  Text(
                    "Hoy es ${dateString[0]}, ${dateString[1]} de ${dateString[2]} de ${dateString[3]}",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Container(
                    color: Colors.transparent,
                    child: Container(
                      decoration: BoxDecoration(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  !isWeekend()
                      ? RaisedButton(
                          child: Text('Fichar'),
                          color: HexColor("#143D68"),
                          textColor: Colors.white,
                          onPressed: () async {
                            if (await BiometricUtil.isBiometricAvailable()) {
                              await BiometricUtil.getListOfBiometricTypes();
                              if (await BiometricUtil.authenticateUser(
                                  context)) {
                                AttendanceService.setAttendance(context)
                                    .then((value) {
                                  setState(() {
                                    attendances = AttendanceService
                                        .getAttendancesFromUser(context);
                                  });
                                });
                              }
                            }
                          },
                          elevation: 5.0,
                        )
                      : Container(),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 220.50),
              height: double.infinity,
              width: double.infinity,
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                    color: HexColor("#F2F4F8"),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(35.0),
                        topRight: Radius.circular(35.0))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(24.0),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Ultimas asistencias',
                            style: TextStyle(
                              color: HexColor("#143D68"),
                              fontSize: 24.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Divider(
                      height: ScreenUtil().setHeight(1.0),
                    ),
                    SingleChildScrollView(
                        child: FutureBuilder<List<Attendance>>(
                      future: attendances,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return CircularProgressIndicator();
                        }
                        List<Attendance> attendances = snapshot.data;

                        attendances = attendances
                          ..sort((a, b) => b.access_at.compareTo(a.access_at));

                        Map attendancesGroupBy = groupBy(attendances,
                            (Attendance obj) => obj.access_at.split(" ")[0]);

                        List<Widget> widgets = new List();

                        attendancesGroupBy.forEach((f, k) {
                          var dateParsed = DateTime.parse(f);
                          var date = DateFormat('EEEE, d MMMM yyyy', 'es_ES')
                              .format(dateParsed);
                          widgets.add(
                            Column(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(
                                      right: 24.0,
                                      left: 24.0,
                                      bottom: 24.0,
                                      top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    '${date[0].toUpperCase()}${date.substring(1)}',
                                    style: TextStyle(
                                      color: Colors.black26,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                          for (var i = 0; i < k.length; i++) {
                            widgets.add(GestureDetector(
                              onTap: () => showAttendanceDetail(k[i]),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: k.length == 1
                                      ? BorderRadius.all(Radius.circular(10.0))
                                      : i == 0
                                          ? BorderRadius.only(
                                              topLeft: Radius.circular(10.0),
                                              topRight: Radius.circular(10.0))
                                          : i == k.length - 1
                                              ? BorderRadius.only(
                                                  bottomLeft:
                                                      Radius.circular(10.0),
                                                  bottomRight:
                                                      Radius.circular(10.0))
                                              : null,
                                ),
                                margin: EdgeInsets.only(
                                  right: 20.0,
                                  left: 20,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(k[i].type == 0
                                          ? "Entrada"
                                          : "Salida"),
                                      subtitle: Text(
                                          "Has salido a las ${k[i].access_at.split(" ")[1].substring(0, 5)}"),
                                      leading: k[i].type == 0
                                          ? Icon(Icons.open_in_browser,
                                              size: 42.0)
                                          : Icon(
                                              Icons.exit_to_app,
                                              size: 42.0,
                                            ),
                                      trailing: Text(k[i]
                                          .access_at
                                          .split(" ")[1]
                                          .substring(0, 5)),
                                    )
                                  ],
                                ),
                              ),
                            ));
                            i != k.length - 1
                                ? widgets.add(Divider(
                                    height: 1.0,
                                    endIndent: 20.0,
                                    indent: 20.0,
                                  ))
                                : null;
                          }
                        });
                        return Container(
                          height: ScreenUtil().setHeight(990.0),
                          child: ListView(
                            children: widgets,
                          ),
                        );
                      },
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
