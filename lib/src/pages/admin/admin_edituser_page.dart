import 'package:app/src/models/User.dart';
import 'package:app/src/services/auth_service.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

class AdminEditUserPage extends StatefulWidget {
  final User user;
  AdminEditUserPage({Key key, this.user}) : super(key: key);
  @override
  _AdminEditUserPageState createState() => _AdminEditUserPageState();
}

class _AdminEditUserPageState extends State<AdminEditUserPage> {
  User user;
  TextEditingController _fullnameController;
  TextEditingController _emailController;

  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    print(form);
    if (form.validate()) {
      form.save();  
      AuthService.updateUser(user.id, _fullnameController.text, _emailController.text, context);
      return true;
    }
    return false;
  }

  @override
  void initState() {
    setState(() {
      user = widget.user;
      _fullnameController = TextEditingController(text: user.name);
      _emailController = TextEditingController(text: user.email);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("27519E"),
        elevation: 0.0,
        title: Text(
          "Cambiar datos de ${user.name}",
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Column(
        children: <Widget>[
          Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 17.0, top: 20.0),
                    child: Text(
                      'Nombre completo',
                      style: TextStyle(
                          fontSize: 17.0, fontWeight: FontWeight.bold),
                    )),
                Container(
                    margin: EdgeInsets.only(
                        top: 15.0, right: 15.0, left: 15.0, bottom: 10.0),
                    child: TextFormField(
                        controller: _fullnameController,
                        validator: (value) => value.isEmpty
                            ? 'Tienes que introducir un nombre'
                            : null,
                        decoration: InputDecoration(
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(ScreenUtil().setWidth(5)),
                          ),
                          hintText: 'Introduce el nombre completo',
                        ),
                        onSaved: (value) {
                          _fullnameController.text = value;
                        })),
                Container(
                    child: Text(
                      'Correo electronico',
                      style: TextStyle(
                          fontSize: 17.0, fontWeight: FontWeight.bold),
                    )),
                Container(
                    margin: EdgeInsets.only(
                        top: 15.0, right: 15.0, left: 15.0, bottom: 10.0),
                    child: TextFormField(
                        controller: _emailController,
                        validator: (value) => value.isEmpty
                            ? 'Tienes que introducir un email'
                            : null,
                        decoration: InputDecoration(
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(ScreenUtil().setWidth(5)),
                          ),
                          hintText: 'Introduce el email',
                        ),
                        onSaved: (value) {
                          _emailController.text = value;
                        })),
              ],
            ),
          ),
          Center(
            child: Container(
              child: MaterialButton(
                child: Text(
                  'Actualizar usuario',
                  style: TextStyle(color: Colors.white),
                ),
                color: Theme.of(context).primaryColor,
                onPressed: () => validateAndSave(),
                elevation: 7.0,
                height: ScreenUtil().setHeight(80.0),
                shape: StadiumBorder(),
              ),
            ),
          ),
          Center(
            child: Container(
              child: MaterialButton(
                child: Text(
                  'Descargar informe',
                  style: TextStyle(color: Colors.white),
                ),
                color: Theme.of(context).primaryColor,
                onPressed: () => AuthService.downloadReport(user.id, context),
                elevation: 7.0,
                height: ScreenUtil().setHeight(80.0),
                shape: StadiumBorder(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
