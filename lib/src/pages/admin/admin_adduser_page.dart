import 'package:app/src/models/User.dart';
import 'package:app/src/services/auth_service.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

class AdminAddUserPage extends StatefulWidget {
  final User user;
  AdminAddUserPage({Key key, this.user}) : super(key: key);
  @override
  _AdminAddUserPageState createState() => _AdminAddUserPageState();
}

class _AdminAddUserPageState extends State<AdminAddUserPage> {
  TextEditingController _fullnameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    print(form);
    if (form.validate()) {
      form.save();  
      AuthService.createUser( _fullnameController.text, _emailController.text, _passwordController.text, context);
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("27519E"),
        elevation: 0.0,
        title: Text(
          "Crear usuario",
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Column(
        children: <Widget>[
          Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 17.0, top: 20.0),
                    child: Text(
                      'Nombre completo',
                      style: TextStyle(
                          fontSize: 17.0, fontWeight: FontWeight.bold),
                    )),
                Container(
                    margin: EdgeInsets.only(
                        top: 15.0, right: 15.0, left: 15.0, bottom: 10.0),
                    child: TextFormField(
                        controller: _fullnameController,
                        validator: (value) => value.isEmpty
                            ? 'Tienes que introducir un nombre'
                            : null,
                        decoration: InputDecoration(
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(ScreenUtil().setWidth(5)),
                          ),
                          hintText: 'Introduce el nombre completo',
                        ),
                        onSaved: (value) {
                          _fullnameController.text = value;
                        })),
                Container(
                    child: Text(
                      'Correo electronico',
                      style: TextStyle(
                          fontSize: 17.0, fontWeight: FontWeight.bold),
                    )),
                Container(
                    margin: EdgeInsets.only(
                        top: 15.0, right: 15.0, left: 15.0, bottom: 10.0),
                    child: TextFormField(
                        controller: _emailController,
                        validator: (value) => value.isEmpty
                            ? 'Tienes que introducir un email'
                            : null,
                        decoration: InputDecoration(
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(ScreenUtil().setWidth(5)),
                          ),
                          hintText: 'Introduce el email',
                        ),
                        onSaved: (value) {
                          _emailController.text = value;
                        })),
                                        Container(
                    child: Text(
                      'Contraseña',
                      style: TextStyle(
                          fontSize: 17.0, fontWeight: FontWeight.bold),
                    )),
                Container(
                    margin: EdgeInsets.only(
                        top: 15.0, right: 15.0, left: 15.0, bottom: 10.0),
                    child: TextFormField(
                        obscureText: true,
                        controller: _passwordController,
                        validator: (value) => value.isEmpty
                            ? 'Tienes que introducir una contraseña'
                            : null,
                        decoration: InputDecoration(
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(ScreenUtil().setWidth(5)),
                          ),
                          hintText: 'Introduce una contraseña',
                        ),
                        onSaved: (value) {
                          _passwordController.text = value;
                        })),
              ],
            ),
          ),
          Center(
            child: Container(
              child: MaterialButton(
                child: Text(
                  'Crear usuario',
                  style: TextStyle(color: Colors.white),
                ),
                color: Theme.of(context).primaryColor,
                onPressed: () => validateAndSave(),
                elevation: 7.0,
                height: ScreenUtil().setHeight(80.0),
                shape: StadiumBorder(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
