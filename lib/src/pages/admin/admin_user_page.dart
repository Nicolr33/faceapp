import 'package:app/src/models/Attendance.dart';
import 'package:app/src/models/User.dart';
import 'package:app/src/pages/admin/admin_edituser_page.dart';
import 'package:app/src/services/admin_service.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:flutter/material.dart';

import 'admin_adduser_page.dart';

class AdminUsersPage extends StatefulWidget {
  @override
  _AdminUsersPageState createState() => _AdminUsersPageState();
}

class _AdminUsersPageState extends State<AdminUsersPage> {
  @override
  
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("27519E"),
        elevation: 0.0,
        title: Text(
          "Usuarios · Panel Admin",
          style: TextStyle(color: Colors.white),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AdminAddUserPage())),
          )
        ],
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        child: FutureBuilder(
          future: AdminService.getUsers(context),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }

            return ListView.builder(
              itemBuilder: (context, index) {
                User user = snapshot.data[index];
                return Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(user.name),
                      subtitle: Text(user.email),
                      leading: CircleAvatar(
                        backgroundImage: NetworkImage(user.img_url != null ? user.img_url : ""),
                        backgroundColor: HexColor("27519E"),
                      ),
                      trailing: Icon(Icons.arrow_right),
                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AdminEditUserPage(user: user,))),
                    ),
                    Divider(height: 1.0,)
                  ],
                );
              },
              itemCount: snapshot.data.length,
            );
          },
        ),
      ),
    );
  }
}