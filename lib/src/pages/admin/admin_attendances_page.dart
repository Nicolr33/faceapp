import 'package:app/src/models/Attendance.dart';
import 'package:app/src/models/User.dart';
import 'package:app/src/services/admin_service.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:flutter/material.dart';

class AdminAttendancesPage extends StatefulWidget {
  @override
  _AdminAttendancesPageState createState() => _AdminAttendancesPageState();
}

class _AdminAttendancesPageState extends State<AdminAttendancesPage> {
  

  @override
  Widget build(BuildContext context) {
    void showAttendanceDetail(Attendance attendance) {
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: context,
        builder: (BuildContext context) {
          return Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                attendance.type == 0
                    ? Icon(
                        Icons.open_in_browser,
                        size: 42.0,
                        color: Colors.grey,
                      )
                    : Icon(
                        Icons.exit_to_app,
                        size: 42.0,
                        color: Colors.grey,
                      ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  child: attendance.type == 0
                      ? Text(
                          "Entrada",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24.0),
                        )
                      : Text(
                          "Salida",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24.0),
                        ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  attendance.access_at.split(" ")[0],
                  style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Divider(
                        height: 1.0,
                        color: Colors.black38,
                      ),
                      ListTile(
                        title: Text(
                          "Usuario: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle:
                            Text(attendance.user.name + " - " + attendance.user.email),
                      ),
                      Divider(
                        height: 1.0,
                        color: Colors.black38,
                      ),
                      ListTile(
                        title: Text(
                          "Comentario: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(attendance.comment == null ||
                                attendance.comment.isEmpty
                            ? "No hay ningun comentario."
                            : attendance.comment),
                      ),
                      Divider(
                        height: 1.0,
                        color: Colors.black38,
                      ),
                      ListTile(
                        title: Text(
                          "Dirección: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(attendance.address == null ||
                                attendance.address.isEmpty
                            ? "No se ha encontrado una dirección valida."
                            : attendance.address),
                      ),
                      Divider(
                        height: 1.0,
                        color: Colors.black38,
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        });
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("27519E"),
        elevation: 0.0,
        title: Text(
          "Asistencias · Panel Admin",
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        child: FutureBuilder(
          future: AdminService.getAttendances(context),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }

            return ListView.builder(
              itemBuilder: (context, index) {
                Attendance attendance = snapshot.data[index];
                return Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => showAttendanceDetail(attendance),
                      child: ListTile(
                        title: Text(attendance.type == 0 ? "Entrada" : "Salida"),
                        subtitle: Text(
                            " ${attendance.user.name} ha ${attendance.type == 0 ? "entrado" : "salido"} a las ${attendance.access_at.split(" ")[1].substring(0, 5)}"),
                        trailing: attendance.type == 0
                            ? Icon(Icons.open_in_browser, size: 42.0)
                            : Icon(
                                Icons.exit_to_app,
                                size: 42.0,
                              ),
                        leading: CircleAvatar(
                            backgroundImage: NetworkImage(attendance.user.img_url),
                            backgroundColor: HexColor("27519E")),
                      ),
                    ),
                    Divider(height: 1.0,)
                  ],
                );
              },
              itemCount: snapshot.data.length,
            );
          },
        ),
      ),
    );
  }
}
