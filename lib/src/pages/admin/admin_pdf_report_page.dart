import 'package:app/src/utils/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:pdf_viewer_plugin/pdf_viewer_plugin.dart';


class AdminPDFReportPage extends StatefulWidget {
  final String path;
  AdminPDFReportPage({Key key, this.path}) : super(key: key);
  @override
  _AdminPDFReportPageState createState() => _AdminPDFReportPageState();
}

class _AdminPDFReportPageState extends State<AdminPDFReportPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("27519E"),
        elevation: 0.0,
        
        title: Text(
          "Informe",
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: PdfViewer(
        filePath: widget.path,
      ),
    );
  }
}