import 'package:app/src/pages/admin/admin_attendances_page.dart';
import 'package:app/src/pages/admin/admin_reports_page.dart';
import 'package:app/src/pages/admin/admin_settings_page.dart';
import 'package:app/src/pages/admin/admin_user_page.dart';
import 'package:app/src/utils/hex_color.dart';
import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flutter/material.dart';

class AdminHomePage extends StatefulWidget {
  @override
  _AdminHomePageState createState() => _AdminHomePageState();
}

class _AdminHomePageState extends State<AdminHomePage> {
  int _selectedIndex = 0;

  List screens = [
    AdminUsersPage(),
    AdminAttendancesPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      bottomNavigationBar: FFNavigationBar(
        theme: FFNavigationBarTheme(
          barBackgroundColor: Colors.white,
          selectedItemBorderColor: HexColor("455D7E"),
          selectedItemBackgroundColor: HexColor("27519E"),
          selectedItemIconColor: Colors.white,
          selectedItemLabelColor: Colors.black,
        ),
        selectedIndex: _selectedIndex,
        onSelectTab: (index) {
          setState(() {
            print(index);
            _selectedIndex = index;
          });
        },
        items: [
          FFNavigationBarItem(
            iconData: Icons.people,
            label: 'Usuarios',
          ),
          FFNavigationBarItem(
            iconData: Icons.assignment_turned_in,
            label: 'Asistencias',
          ),
        ],
      ),
      body: screens[_selectedIndex],
    );
  }
}
