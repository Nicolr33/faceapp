import 'package:app/routes.dart';
import 'package:flutter/material.dart'; 



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Gestion Poblenou',
      initialRoute: 'splash',
      routes: Routes.routes,
    );
  }
}
