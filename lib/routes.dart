import 'package:app/src/pages/admin/admin_home_page.dart';
import 'package:app/src/pages/auth/login_page.dart';
import 'package:app/src/pages/auth/splash_loading_page.dart';
import 'package:app/src/pages/home/home_page.dart';
import 'package:app/src/pages/settings/password_page.dart';
import 'package:app/src/pages/settings/settings_page.dart';
import 'package:flutter/material.dart';

class Routes {
  static var routes = <String, WidgetBuilder>{
    'login': (BuildContext context) => LoginPage(),
    'home': (BuildContext context) => HomePage(),
    'splash': (BuildContext context) => SplashLoadingPage(),
    'settings': (BuildContext context) => SettingsPage(),
    'password': (BuildContext context) => PasswordPage(),
    'adminhome': (BuildContext context) => AdminHomePage()
  };
}
